part of 'notifications_bloc.dart';

@freezed
class NotificationsEvent with _$NotificationsEvent {
  const factory NotificationsEvent.fetchNotifications() = _FetchNotifications;

  const factory NotificationsEvent.removeNotification(int notificationId) =
      _RemoveNotification;

  const factory NotificationsEvent.updateNotifications({
    required bool showLoading,
  }) = _UpdateNotifications;
}
